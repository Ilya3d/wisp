Wisp
====

Web app experiments


# Local run

* Edit config file
```
# .env file
DATABASE_URL=mysql://wisp:secret@db:3306/wisp
```

* Build and run container
```sh
composer run-script docker:build -d source
```

* Clean containers
```sh
composer run-script docker:cleanup -d source
```